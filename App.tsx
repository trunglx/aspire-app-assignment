import { StatusBar } from 'expo-status-bar';
import React, { useEffect } from 'react';
import { ActivityIndicator, Platform, UIManager, View } from 'react-native';
import { enableScreens } from 'react-native-screens';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import RootNavigator from './src/navigation/RootNavigator';
import { persistor, store } from './src/redux/store';
import { RootSiblingParent } from "react-native-root-siblings"

export default function App() {

  useEffect(() => {
    console.log("run setup")
    // React Navigation, optimize memory usage.
    enableScreens()

    // Layout animation
    if (Platform.OS === "android") {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true)
      }
    }
  }, [])

  return (
    <Provider store={store}>
      <PersistGate loading={<ActivityIndicator />} persistor={persistor}>
        <View style={{ flex: 1 }}>
          <StatusBar style="light" />
          <RootSiblingParent>
            <RootNavigator />
          </RootSiblingParent>
        </View>
      </PersistGate>
    </Provider>
  );
}
