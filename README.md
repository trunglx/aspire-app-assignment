# Aspire App Assignment
## Project description:
This is an assignment task from Aspire, that the design UI from xd: https://xd.adobe.com/view/9a3f7272-f4c1-4e65-9287-89ad0f353406-cf6c/grid 
This app is a part of fully function app, that help users manage their Debit card and set the weekly spending limit on card.

## Features

- Users can load their account info from server, with info:
    - Available Balance
    - Card Info
    - Weekly spending limit
    - Weekly spending limit exauted.
- With card info, it required:
   - Default hide the info of card number and card cvv number.
   - User can toggle show/hide card number and cvv
- User can scroll card down or up to view the menu item function of card
- User can set and see the current weekly spending limit:
    - Show exauted weekly in spending
    - Go to set weekly spending limit screen if not set
    - Remove weekly spending limit if have set
- User can choose default set spending limit or input the value but minimum is 50$

## Tech

Some dependencies using:

- [Expo] - Framework and a platform for universal react application.
- [Type Script] - Manage syntax and type with typescript
- [Jest Test] - unit test 
- [React Navigation] - Build navigation and bottom navigation
- [Redux Toolkit] - powerful tool to manage redux with slice, avoid boiler blate code of redux
- [Redux Saga] - Process async function return with redux
- [Redux Saga Testing] - Testing actions from Saga
- [Reactotron Redux] - Debug store data of redux
- [React native elements] - provide component base on material design

## Installation

Requires [Node.js](https://nodejs.org/) v16+ to run.

Install the dependencies and devDependencies and start to devices.

```sh
git clone https://gitlab.com/trunglx/aspire-app-assignment.git
cd aspire-app-assignment
yarn install
yarn android --Run Android
yarn ios --Run ios
```

## Screenshot
![alt text](https://www.linkpicture.com/q/Screenshot_1635705078.png)
![alt text](https://www.linkpicture.com/q/Screenshot_1635705160.png)
![alt text](https://www.linkpicture.com/q/Screenshot_1635705162.png)
![alt text](https://www.linkpicture.com/q/Screenshot_1635705168.png)
![alt text](https://www.linkpicture.com/q/Screenshot_1635705170.png)


