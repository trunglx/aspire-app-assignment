interface AccountInfo {
    availableBalance: number,
    weeklySpendingLimit: number | undefined,
    weeklySpendingLimitExhausted: number,
    cardInfo: CardInfo | undefined,
}