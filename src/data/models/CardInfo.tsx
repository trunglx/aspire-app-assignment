interface CardInfo {
    holderName: string,
    cardNumber: string,
    validDate: string,
    cardCVV: string,
}