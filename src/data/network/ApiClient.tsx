import HttpException from "./exceptions/HttpException"

type ContentType = "JSON" | "Text" | "Unsupported"

interface ApiConfig {
  url: string
  method?: "GET" | "PUT" | "POST" | "DELETE"
  body?: object
  timeoutInSeconds?: number
}

const defaultTimeout = 10
const BASE_URL =
  "https://617ecc162ff7e600174bd909.mockapi.io/api"

const getHttpConfig = (
  method: ApiConfig["method"] = "GET",
  body?: object,
): RequestInit => {
  const base = {
    method: method,
    headers: {
      accept: "application/json",
      "content-type": "application/json",
    },
  }

  const withBody = body ? { ...base, body: JSON.stringify(body) } : base

  return withBody
}

const getContentType = (res: Response): ContentType => {
  const isJSON =
    res.headers.get("Content-Type")?.startsWith("application/json") || false

  if (isJSON) return "JSON"

  const isText = res.headers.get("Content-Type")?.startsWith("text") || false

  if (isText) return "Text"

  return "Unsupported"
}

const doThrow = async (res: Response, contentType: ContentType) => {
  // Not 2XX
  if (contentType === "JSON") {
    const { error: error } = await res.json()
    throw new HttpException(error.code, error.message, res.url)
  } else if (contentType === "Text") {
    const errorText = await res.text()
    throw new HttpException(res.status, errorText, res.url)
  }

  // Not 2XX, not JSON and not text.
  throw new HttpException(res.status, "Unsupported content type", res.url)
}

const processResponse = async (res: Response) => {
  const contentType = getContentType(res)
  // HTTP 2XX
  if (res.ok) {
    if (contentType === "JSON") {
      return await res.json()
    } else {
      return await res.text()
    }
  }

  return doThrow(res, contentType)
}

const makeRequest = async <T,>({
  url,
  method,
  body,
  timeoutInSeconds,
}: ApiConfig) => {
  // if (NetworkHelper.isInternetReachable) {
  const reqConfig = await getHttpConfig(method, body)

  const fullURL = BASE_URL + url
  const reqTimeout = timeoutInSeconds || defaultTimeout

  const controller = new AbortController()
  const finalConfig = { signal: controller.signal, ...reqConfig }

  const abort = setTimeout(() => {
    controller.abort()
  }, reqTimeout * 1000)
  console.log(body)
  const res = await fetch(fullURL, finalConfig)

  clearTimeout(abort)

  return processResponse(res) as Promise<T>
  // } else {
  //   throw new OfflineException("Offline", "Internet not reachable", url)
  // }
}

export const ApiClient = {
  get: <T,>({ url, body, timeoutInSeconds }: Omit<ApiConfig, "method">) =>
    makeRequest({ url: url, method: "GET", body, timeoutInSeconds }),
  post: <T,>({ url, body, timeoutInSeconds }: Omit<ApiConfig, "method">) =>
    makeRequest<T>({ url: url, method: "POST", body, timeoutInSeconds }),
  put: <T,>({ url, body, timeoutInSeconds }: Omit<ApiConfig, "method">) =>
    makeRequest<T>({ url: url, method: "PUT", body, timeoutInSeconds }),
  delete: <T,>({ url, body, timeoutInSeconds }: Omit<ApiConfig, "method">) =>
    makeRequest<T>({ url: url, method: "DELETE", body, timeoutInSeconds }),
}
