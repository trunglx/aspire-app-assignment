import { ApiClient } from "./ApiClient";
import { ApiEndPoints } from "./ApiEndpoints";
import { AccountInfoResponse } from "./response/AccountInfoResponse";

export const ApiServices = {
    getMyAccountInfo: () => ApiClient.get<AccountInfoResponse>({
        url: ApiEndPoints.myAccountInfo
    }),
    setWeeklySpendingLimit: (value: number) => ApiClient.post({
        url: ApiEndPoints.setWeeklySpendingLimit,
        body: {
            value
        }
    }),
}