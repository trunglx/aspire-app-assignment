import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Dimensions, Image } from 'react-native'
import { Button } from 'react-native-elements';
import { useDispatch, useSelector, useStore } from 'react-redux';
import { Images, Colors } from '../../commons';
import { dimensions } from '../../commons/Dimension';
// import { selectAppColorSolid } from '../store/slices/appVariablesSlice';
// import { selectCardCVV, selectCardNumber, selectCardNumberVisible, selectCardValidThru, selectNameOnCard, setCardNumberVisible } from '../store/slices/debitCardSlice';

const CARD_WIDTH = (dimensions.width - 48);
const CARD_HEIGHT = 0.6 * CARD_WIDTH;

type Props = {
    cardInfo: CardInfo | undefined
}

const CardView = ({ cardInfo }: Props) => {

    const [isShowCardNumber, setShowCardNumber] = useState(false)

    return (
        <View style={{ backgroundColor: 'transparent', width: CARD_WIDTH, height: CARD_HEIGHT + 32, marginTop: -90 }}>
            <View style={{ backgroundColor: 'white', alignSelf: 'flex-end', width: 151, height: 45, borderTopRightRadius: 6, borderTopLeftRadius: 6 }}>
                <Button
                    type="clear"
                    title={isShowCardNumber ? "Hide card number" : "Show card number"}
                    titleStyle={{
                        color: Colors.green_app_color,
                        fontSize: 12,
                        fontWeight: "600",
                    }}
                    icon={
                        <Image
                            style={styles.iconImage}
                            source={(isShowCardNumber) ? Images.ic_hide_number : Images.ic_show_number}
                            resizeMode='contain'
                        />
                    }
                    onPress={() => {
                        setShowCardNumber(!isShowCardNumber)
                    }}
                />
            </View>
            <View style={{ backgroundColor: Colors.green_app_color, width: CARD_WIDTH, height: CARD_HEIGHT, borderRadius: 10, marginTop: -13, padding: 0, ...styles.shadow, zIndex: 9999 }}>
                <View style={{ marginTop: 24, height: 21, marginRight: 24, alignItems: 'flex-end' }}>
                    <Image
                        style={{ width: 74 }}
                        source={Images.logo_with_text}
                        resizeMode='contain'
                    />
                </View>
                <View style={{ height: CARD_HEIGHT - 89, flexDirection: 'column', alignContent: 'space-between' }}>
                    <View style={{ flex: 1, justifyContent: 'center', marginLeft: 24 }}>
                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 22 }}>{cardInfo?.holderName}</Text>
                    </View>
                    <View style={{ flex: 1, marginLeft: 24, alignContent: 'space-between' }}>
                        <View>
                            <Text>
                                {_renderCardNumber(cardInfo?.cardNumber, isShowCardNumber)}
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 20 }}>
                            <Text style={{ color: 'white', fontWeight: '600', fontSize: 13 }}>Thru: {cardInfo?.validDate}</Text>
                            <Text style={{ color: 'white', fontWeight: '600', fontSize: 13, marginLeft: 24 }}>CVV: {isShowCardNumber ? cardInfo?.cardCVV : '***'}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ marginBottom: 24, marginRight: 24, height: 20, alignItems: 'flex-end' }}>
                    <Image
                        style={{ width: 59 }}
                        source={Images.visa_logo}
                        resizeMode='contain'
                    />
                </View>
            </View>
            <View style={styles.borderBehindView} />
        </View>
    )
}


const _renderCardNumber = (cardNumber: string | undefined, isShowNumber: boolean) => {
    const bulletView = () => <View style={styles.bullets} />

    if (!(isShowNumber != null && cardNumber != null)) {
        return (<View style={{ display: 'none' }} />);
    }
    if (isShowNumber)
        return (
            <View style={{ flexDirection: 'row' }}>
                <Text style={{ color: 'white', fontWeight: '600', fontSize: 16, width: 50, letterSpacing: 2 }}>{cardNumber.substring(0, 4)}</Text>
                <Text style={{ color: 'white', fontWeight: '600', fontSize: 16, marginLeft: 20, width: 50, letterSpacing: 2 }}>{cardNumber.substring(4, 8)}</Text>
                <Text style={{ color: 'white', fontWeight: '600', fontSize: 16, marginLeft: 20, width: 50, letterSpacing: 2 }}>{cardNumber.substring(8, 12)}</Text>
                <Text style={{ color: 'white', fontWeight: '600', fontSize: 16, marginLeft: 20, width: 50, letterSpacing: 2 }}>{cardNumber.substring(12, 16)}</Text>
            </View>
        );
    else {
        return (
            <View style={{ flexDirection: 'row' }}>
                {bulletView()}
                {bulletView()}
                {bulletView()}
                {bulletView()}
                <View style={{ width: 24 }} />
                {bulletView()}
                {bulletView()}
                {bulletView()}
                {bulletView()}
                <View style={{ width: 24 }} />
                {bulletView()}
                {bulletView()}
                {bulletView()}
                {bulletView()}
                <View style={{ width: 24 }} />
                <Text style={{ color: 'white', fontWeight: '600', fontSize: 16, letterSpacing: 2 }}>{cardNumber.substring(12, 16)}</Text>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    iconImage: {
        height: 16,
        width: 16,
        marginRight: 6,
    },
    bullets: {
        height: 8,
        width: 8,
        borderRadius: 8,
        marginTop: 8,
        marginRight: 6,
        backgroundColor: 'white',
    },
    shadow: {
        shadowColor: '#AAA',
        shadowOffset: {
            width: 5,
            height: 5,
        },
        shadowOpacity: 0.5,
        shadowRadius: 3.5,
        elevation: 8,
    },
    borderBehindView: {
        backgroundColor: 'white',
        height: CARD_HEIGHT,
        marginTop: -CARD_HEIGHT + 52,
        marginLeft: -24,
        marginRight: -24,
        borderTopRightRadius: 18,
        borderTopLeftRadius: 18,
    }
})

export default CardView