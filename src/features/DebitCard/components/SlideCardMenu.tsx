import React from "react";
import { FlatList, View, Text, TouchableOpacity, Image, StyleSheet } from "react-native";
import { Images } from "../../commons";
import { dimensions } from "../../commons/Dimension";
import CardView from "./CardView";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { selectAccountInfo, selectCardInfo, setWeeklySpendingLimit } from "../../../redux/slices/accounInfoSlice";
import SpendingLimitProgress from "./SpendingLimitProgress";
import UTIL from "../../commons/util";

export default function SlideCardMenu() {

    const navigation = useNavigation();

    const accountUserInfo = useSelector(selectAccountInfo)

    const dispatch = useDispatch()

    let menuItems = [
        {
            key: "MenuItem#1",
            menuTitle: "Top-up account",
            menuSubtitle: "Deposit money to your account to use with card",
            iconAssetUri: Images.ic_menu_top_up,
            buttonState: -1,
            itemEnabled: false,
        },
        {
            key: "MenuItem#2",
            menuTitle: "Weekly spending limit",
            menuSubtitle: accountUserInfo.weeklySpendingLimit ? "Your weekly spending limit is S$" + UTIL.formatCurrency(accountUserInfo.weeklySpendingLimit) : "You haven't set any spending limit on card",
            iconAssetUri: Images.ic_menu_spending_limit,
            buttonState: accountUserInfo.weeklySpendingLimit ? 1 : 0,
            itemEnabled: true,
        },
        {
            key: "MenuItem#3",
            menuTitle: "Freeze card",
            menuSubtitle: "Your debit card is currently active",
            iconAssetUri: Images.ic_menu_freeze_card,
            buttonState: 0,
            itemEnabled: false,
        },
        {
            key: "MenuItem#4",
            menuTitle: "Get a new card",
            menuSubtitle: "This deactivates your current debit card",
            iconAssetUri: Images.ic_menu_new_card,
            buttonState: -1,
            itemEnabled: false,
        },
        {
            key: "MenuItem#5",
            menuTitle: "Deactivated cards",
            menuSubtitle: "Your previously deactivated cards",
            iconAssetUri: Images.ic_menu_deactive_card,
            buttonState: -1,
            itemEnabled: false,
        }
    ];
    const _renderRadioButton = (buttonState: number, isEnable: boolean) => {
        switch (buttonState) {
            case -1:
                return <View />
            case 0:
            case 1:
                return <TouchableOpacity
                    onPress={() => {
                        if (accountUserInfo.weeklySpendingLimit) {
                            dispatch(setWeeklySpendingLimit(null))
                        } else {
                            navigation.navigate('SpendingLimit')
                        }
                    }}
                    disabled={!isEnable}
                ><Image
                        style={{
                            width: 34,
                            height: 20,
                        }}
                        source={buttonState == 0 ? Images.toggle_inactive : Images.toggle_active}
                        resizeMode='contain'
                    />
                </TouchableOpacity>
        }
    }

    return (
        <FlatList
            style={{
                alignSelf: 'stretch',
            }}
            bounces={true}
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={
                <View>
                    <View style={{ alignItems: 'center' }}>
                        <View style={{ flex: 1, height: dimensions.height * 0.4 }} />
                        <CardView cardInfo={accountUserInfo.cardInfo} />
                    </View>
                    <SpendingLimitProgress />
                </View>
            }
            ListFooterComponent={
                <View style={{ height: 24, backgroundColor: 'white' }} />
            }
            data={menuItems}
            renderItem={({ item }) => {
                return (
                    <View style={{ backgroundColor: 'white', zIndex: -100 }}>
                        <View style={{ marginLeft: 24, marginRight: 24 }}>

                            <View style={styles.menuItem}>
                                <Image
                                    style={{ width: 32 }}
                                    source={item.iconAssetUri}
                                    resizeMode='contain'
                                />
                                <View style={{ flexDirection: 'column', marginLeft: 12 }}>
                                    <Text style={styles.menuTitle}>{item.menuTitle}</Text>
                                    <Text style={styles.menuSubtitle}>{item.menuSubtitle}</Text>
                                </View>
                                <View style={{
                                    alignItems: 'flex-end',
                                    flex: 1
                                }}>
                                    {_renderRadioButton(item.buttonState, item.itemEnabled)}
                                </View>
                            </View>
                        </View>
                    </View>
                );
            }}
        />
    )
}

const styles = StyleSheet.create({
    menuItem: {
        flexDirection: 'row',
        height: 41,
        marginTop: 22,
        alignContent: 'center',
        alignItems: 'center'
    },
    menuTitle: {
        height: 19,
        fontWeight: '400',
        fontSize: 14,
        alignContent: 'flex-start',
        flex: 1,
        marginBottom: 2,
    },
    menuIcon: {
        width: 32,
        height: 32,
    },
    menuSubtitle: {
        height: 18,
        fontWeight: '300',
        fontSize: 12,
        color: 'rgba(34,34,34,0.4)'
    },
})