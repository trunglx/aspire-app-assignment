import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { LinearProgress } from "react-native-elements";
import { useSelector } from "react-redux";
import { selectAccountInfo } from "../../../redux/slices/accounInfoSlice";
import { Colors } from "../../commons";
import UTIL from "../../commons/util";

export default function SpendingLimitProgress() {
    const accountInfo = useSelector(selectAccountInfo)
    return (
        accountInfo.weeklySpendingLimit ?
            <View style={{ backgroundColor: 'white' }}>
                <View style={{ height: 39, marginLeft: 24, marginRight: 24, marginTop: 24 }}>
                    <View style={styles.rowText}>
                        <Text style={styles.titleText}>Debit card spending limit</Text>
                        <View style={{ alignSelf: 'flex-end', flexDirection: 'row' }}>
                            <Text style={styles.exautedText}>{'$' + UTIL.formatCurrency(accountInfo.weeklySpendingLimitExhausted)}</Text>
                            <Text style={styles.limitText}> | {'$' + UTIL.formatCurrency(accountInfo.weeklySpendingLimit)}</Text>
                        </View>
                    </View>
                    <LinearProgress
                        color={Colors.green_app_color}
                        trackColor={Colors.green_app_color + '10'}
                        variant='determinate'
                        value={accountInfo.weeklySpendingLimitExhausted / (accountInfo.weeklySpendingLimit || 1)}
                        style={{
                            height: 15,
                            borderRadius: 30,
                            marginBottom: 0,
                        }}
                    />
                </View>
            </View> :
            null
    )
}

const styles = StyleSheet.create({
    rowText: {
        marginBottom: 6,
        flexDirection: 'row',
        marginLeft: 0,
        marginRight: 0,
        alignContent: 'space-between'
    },
    titleText: {
        fontWeight: '600',
        fontSize: 13,
        alignSelf: 'flex-start',
        flex: 1
    },
    exautedText: {
        color: Colors.green_app_color,
        fontWeight: '700',
        fontSize: 13
    },
    limitText: {
        color: '#22222233',
        fontSize: 13
    }
})