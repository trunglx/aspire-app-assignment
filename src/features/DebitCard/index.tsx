import React, { useEffect } from "react";
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { RootStackParamList } from "../../navigation/RootStackParamList";
import { View, Image, Text, FlatList, Dimensions, TouchableOpacity } from "react-native";
import styles from "./styles";
import { Colors, Images } from "../commons";
import CardView from "./components/CardView";
import SlideCardMenu from "./components/SlideCardMenu";
import { fetchAccountInfo, selectAccountInfo } from "../../redux/slices/accounInfoSlice";
import { useDispatch, useSelector } from "react-redux";
import UTIL from "../commons/util";

type Props = NativeStackScreenProps<RootStackParamList, 'MainTab'>;

export default function DebitCardScreen({ navigation }: Props) {

    const dispatch = useDispatch()
    const accountInfo = useSelector(selectAccountInfo)


    useEffect(() => {
        dispatch(fetchAccountInfo())
    }, [])

    return (
        <View style={styles.container}>
            <View>
                <View style={{ marginTop: 24, marginHorizontal: 24 }}>
                    <View style={styles.iconLogo}>
                        <Image
                            style={{
                                width: 25,
                                height: 25,
                                resizeMode: "contain",
                            }}
                            source={Images.logo_icon_only}
                        />
                    </View>
                    <Text style={styles.titleDebitCard}>Debit Card</Text>
                    <Text style={{ color: 'white', fontSize: 14, marginTop: 22 }}>Available balance</Text>
                    <View style={styles.containerBalance}>
                        <View style={styles.currencyText}>
                            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 12 }}>
                                S$
                            </Text>
                        </View>
                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 24, marginLeft: 10 }}>
                            {UTIL.formatCurrency(accountInfo.availableBalance)}
                        </Text>
                    </View>
                </View>
            </View>

            <View style={styles.containerListMenu}>
                <SlideCardMenu />
            </View>
        </View>
    )
}