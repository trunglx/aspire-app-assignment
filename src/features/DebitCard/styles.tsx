import { StyleSheet } from 'react-native';
import { Colors } from '../commons';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary_color,
    },
    iconLogo: {
        alignItems: 'flex-end',
    },
    titleDebitCard: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 24
    },
    containerBalance: {
        marginTop: 12,
        flexDirection: 'row',
        alignItems: 'center'
    },
    currencyText: {
        backgroundColor: Colors.green_app_color,
        width: 40,
        height: 22,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center'
    },
    menuItem: {
        flexDirection: 'row',
        height: 41,
        marginTop: 22,
        alignContent: 'center',
        alignItems: 'center'
    },
    menuTitle: {
        height: 19,
        fontWeight: '400',
        fontSize: 14,
        alignContent: 'flex-start',
        flex: 1,
        marginBottom: 2,
    },
    menuIcon: {
        width: 32,
        height: 32,
    },
    menuSubtitle: {
        height: 18,
        fontWeight: '300',
        fontSize: 12,
        color: 'rgba(34,34,34,0.4)'
    },
    containerListMenu: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        bottom: 0,
        backgroundColor: 'transparent',
        width: '100%',
        flex: 1,
    }
})