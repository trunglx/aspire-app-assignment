import { NativeStackScreenProps } from "@react-navigation/native-stack"
import React, { useState } from "react"
import { TouchableOpacity, View, Image, Text, TextInput, Alert } from "react-native";
import { Button } from "react-native-elements";
import { useDispatch } from "react-redux";
import { RootStackParamList } from "../../navigation/RootStackParamList";
import { setWeeklySpendingLimit } from "../../redux/slices/accounInfoSlice";
import { Colors, Images } from "../commons";
import UTIL from "../commons/util";
import styles from "./styles";

const arrDefault = ["5000", "10000", "20000"]

type Props = NativeStackScreenProps<RootStackParamList, 'SpendingLimit'>;

export default function SpendingLimitScreen({ navigation }: Props) {

    const dispatch = useDispatch()

    const [valueLimit, setValueLimit] = useState("")

    return (
        <View style={styles.container}>
            <View>
                <View style={{ flexDirection: 'column', marginTop: 24, marginHorizontal: 24 }}>
                    <View style={{ flexDirection: 'row', alignContent: 'space-between' }}>
                        <TouchableOpacity
                            onPress={() => {
                                navigation.pop()
                            }}
                        >
                            <Image
                                style={{
                                    width: 25,
                                    height: 25,
                                    resizeMode: "contain",
                                    marginLeft: 0,
                                }}
                                source={Images.ic_back}
                            />
                        </TouchableOpacity>
                        <View style={{ flex: 1 }}>
                        </View>
                        <View>
                            <Image
                                style={{
                                    width: 25,
                                    height: 25,
                                    resizeMode: "contain",
                                }}
                                source={Images.logo_icon_only}
                            />
                        </View>
                    </View>
                    <Text style={styles.titleText}>Spending limit</Text>
                </View>
            </View>
            <View style={{ marginTop: 32, backgroundColor: 'white', flex: 1, borderTopLeftRadius: 24, borderTopRightRadius: 24, paddingHorizontal: 24 }}>
                <View style={{ marginTop: 32, flexDirection: 'column' }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image
                            style={{
                                width: 16,
                                height: 16,
                                resizeMode: "contain",
                            }}
                            source={Images.ic_set_spending_limit}
                        />
                        <Text style={{ fontSize: 13, fontWeight: '400', marginLeft: 8 }}>Set a weekly debit card spending limit</Text>
                    </View>
                    <View style={{ marginTop: 12, flexDirection: 'row', alignItems: 'center', borderBottomWidth: 0.5, borderColor: '#E5E5E5', paddingBottom: 8 }}>
                        <View style={{ backgroundColor: Colors.green_app_color, borderRadius: 3, width: 40, height: 24, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ color: '#fff', fontSize: 16, fontWeight: '700' }}>S$</Text>
                        </View>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={(value) => setValueLimit(value)}
                            value={UTIL.formatCurrency(valueLimit)}
                            underlineColorAndroid={"transparent"}
                            keyboardType="numeric"
                            returnKeyType='done'
                        />
                    </View>
                    <Text style={{ fontSize: 13, fontWeight: '300', color: '#22222266', marginTop: 16 }}>Here weekly means the last 7 days - not the calendar week</Text>
                </View>
                <View style={{ flexDirection: 'row', alignContent: 'space-between', marginTop: 32 }}>
                    {
                        arrDefault.map((item, index) => (
                            <View key={index} style={{ flex: 1, alignContent: 'center', alignItems: 'center' }}>
                                <Button
                                    title={"S$ " + UTIL.formatCurrency(item)}
                                    buttonStyle={styles.buttonPreset}
                                    titleStyle={{ color: Colors.green_app_color }}
                                    onPress={() => {
                                        setValueLimit(item)
                                    }}
                                />
                            </View>

                        ))
                    }
                </View>
                <View style={{ flex: 1 }} />
                <Button
                    title={"Save"}
                    buttonStyle={styles.buttonSubmit}
                    titleStyle={{ color: "#FFF", fontSize: 16 }}
                    disabled={valueLimit.length == 0}
                    onPress={() => {
                        if (Number(valueLimit) >= 50) {
                            dispatch(setWeeklySpendingLimit(valueLimit))
                            navigation.pop()
                        } else {
                            Alert.alert("Error", "Minimum limit is S$50")
                        }
                    }}
                />
            </View>
        </View>
    )
}