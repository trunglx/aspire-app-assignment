import { StyleSheet } from 'react-native';
import { Colors } from '../commons';
import { dimensions } from '../commons/Dimension';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.primary_color,
    },
    iconLogo: {
        alignItems: 'flex-end',
    },
    titleText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 24,
        marginTop: 24
    },
    textInput: {
        marginLeft: 12,
        borderWidth: 0,
        fontWeight: 'bold',
        fontSize: 24,
        flex: 1,
        alignContent: 'center',
        justifyContent: 'flex-start',
    },

    buttonPreset: {
        borderRadius: 4,
        height: 40,
        width: (dimensions.width - 48 - 32) / 3,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.green_app_color + '0D'
    },
    buttonSubmit: {
        marginBottom: 30,
        height: 56,
        marginLeft: 33,
        marginRight: 33,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center', backgroundColor: Colors.green_app_color
    }
})