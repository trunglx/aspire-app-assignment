import React, { Children } from "react"
import RootSiblings from "react-native-root-siblings"
import { LoadingSpinner } from "./LoadingSpinner"

class LoadingManager {
  loading: RootSiblings | undefined
  constructor() {
  }

  showLoading(): void {
    this.hideLoading()
    this.loading = new RootSiblings(<LoadingSpinner visible={true} />)
  }

  hideLoading() {
    this.loading && this.loading.update(<LoadingSpinner visible={false} />)
  }
}

export default new LoadingManager()
