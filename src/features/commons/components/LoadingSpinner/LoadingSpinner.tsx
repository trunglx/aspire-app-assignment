import React from "react"
import {
  Dialog,
  Text,
  Divider,
  Overlay,
} from "react-native-elements"
import { View, StyleSheet, Modal, ActivityIndicator } from "react-native"
import { Colors } from "../.."

type Props = {
  visible: boolean
}
export const LoadingSpinner = (props: Props) => {
  const [visible, setVisible] = React.useState(true)

  const showDialog = () => setVisible(true)

  const hideDialog = () => setVisible(false)

  return (
    <Overlay isVisible={props.visible}>
      <View style={styles.container}>
        <View style={styles.content}>
          <ActivityIndicator color={Colors.green_app_color} size="large" />
          <View style={{ width: 16 }} />
          <Text style={styles.title}>Loading...</Text>
        </View>
      </View>
    </Overlay >
  )
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  content: {
    borderRadius: 8,
    flexDirection: "row",
    padding: 24,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 18,
  },
  loading: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
})
