const UTIL = {
    formatCurrency(value) {
        if (value == undefined || value == null) {
            return "";
        }
        value = value.toString().replace(",", "")
        if (/\./.test(value)) {
            return value.toString().replace(/\d(?=(\d{3})+\.)/g, "$&,");
        }
        return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    },
}
export default UTIL;