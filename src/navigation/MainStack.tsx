import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { RootStackParamList } from './RootStackParamList';
import DebitCardScreen from '../features/DebitCard';
import Tabs from './MainTab';
import SpendingLimitScreen from '../features/SpendingLimit';


const Stack = createNativeStackNavigator<RootStackParamList>();

export default function MainStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="MainTab" component={Tabs} options={{ headerShown: false }} />
            <Stack.Screen name="SpendingLimit" component={SpendingLimitScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
}