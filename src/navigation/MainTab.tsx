import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import DebitCardScreen from '../features/DebitCard';
import { Colors, Images } from '../features/commons';

const Tab = createBottomTabNavigator();
const Tabs = () => {
    return (
        <Tab.Navigator
            screenOptions={{
                headerShown: false,
                tabBarShowLabel: false,
            }}
            style={styles.shadow}
            backBehavior='none'
            initialRouteName='Debit Card'
        >
            {/* Assigning all tabs to redirect to DebitControlCenterScreen since it is the only screen shared with us*/}
            <Tab.Screen
                name="Home"
                component={DebitCardScreen}
                listeners={{
                    tabPress: e => {
                        // Prevent default action since this item is disabled
                        e.preventDefault();
                    },
                }}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Image
                                source={Images.ic_tab_home}
                                resizeMode='contain'
                                style={{
                                    width: 24,
                                    height: 24,
                                }}
                            />
                            <Text style={{ fontSize: 9, color: "#DDDDDD" }}>Home</Text>
                            {/* These elements will always be set to gray since they are not supposed to be focused as per the shared SR Doc */}
                        </View>
                    ),
                }}
            />
            <Tab.Screen
                name="Debit Card"
                component={DebitCardScreen}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Image
                                source={Images.ic_tab_card}
                                resizeMode='contain'
                                style={{
                                    width: 24,
                                    height: 24,
                                    tintColor: focused ? Colors.green_app_color : "#DDD",
                                }}
                            />
                            <Text style={{ fontSize: 9, color: focused ? Colors.green_app_color : "#DDD", }}>Debit Card</Text>
                            {/* These elements will always be set to gray since they are not supposed to be focused as per the shared SR Doc */}
                        </View>
                    ),
                }}
            />
            <Tab.Screen name="Payments" component={DebitCardScreen}
                listeners={{
                    tabPress: e => {
                        // Prevent default action since this item is disabled
                        e.preventDefault();
                    },
                }}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Image
                                source={Images.ic_tab_payment}
                                resizeMode='contain'
                                style={{
                                    width: 24,
                                    height: 24,
                                    tintColor: "#DDDDDD",
                                }}
                            />
                            <Text style={{ fontSize: 9, color: "#DDDDDD" }}>Payments</Text>
                            {/* These elements will always be set to gray since they are not supposed to be focused as per the shared SR Doc */}
                        </View>
                    )
                }} />
            <Tab.Screen name="Credit" component={DebitCardScreen}
                listeners={{
                    tabPress: e => {
                        // Prevent default action since this item is disabled
                        e.preventDefault();
                    },
                }}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Image
                                source={Images.ic_tab_credit}
                                resizeMode='contain'
                                style={{
                                    width: 24,
                                    height: 24,
                                }}
                            />
                            <Text style={{ fontSize: 9, color: "#DDDDDD" }}>Credit</Text>
                            {/* These elements will always be set to gray since they are not supposed to be focused as per the shared SR Doc */}
                        </View>
                    )
                }} />
            <Tab.Screen name="Profile" component={DebitCardScreen}
                listeners={{
                    tabPress: e => {
                        // Prevent default action since this item is disabled
                        e.preventDefault();
                    },
                }}
                options={{
                    tabBarIcon: ({ focused }) => (
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Image
                                source={Images.ic_tab_profile}
                                resizeMode='contain'
                                style={{
                                    width: 24,
                                    height: 24,
                                }}
                            />
                            <Text style={{ fontSize: 9, color: "#DDDDDD" }}>Profile</Text>
                            {/* These elements will always be set to gray since they are not supposed to be focused as per the shared SR Doc */}
                        </View>
                    )
                }} />
        </Tab.Navigator>
    )
}

export default Tabs

const styles = StyleSheet.create({
    shadow: {
        shadowColor: '#7F5DF0',
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.5,
        elevation: 5,
    }
})
