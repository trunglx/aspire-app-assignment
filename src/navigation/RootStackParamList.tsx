export type RootStackParamList = {
    MainTab: undefined, // undefined because you aren't passing any params to the Main screen
    SpendingLimit: undefined;
};