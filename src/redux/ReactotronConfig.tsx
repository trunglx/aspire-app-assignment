import AsyncStorage from '@react-native-async-storage/async-storage';
import Reactotron, { trackGlobalErrors, networking } from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';

const reactotron = Reactotron
  .setAsyncStorageHandler!(AsyncStorage)
  .configure({
    name: "---My Application---",
  })
  .useReactNative({
    asyncStorage: true, // there are more options to the async storage.
    networking: {
      // optionally, you can turn it off with false.
      ignoreUrls: /\/(logs|symbolicate)$/,
    },
    editor: false, // there are more options to editor
    errors: { veto: (stackFrame) => false }, // or turn it off with false
    overlay: false, // just turning off overlay
  })
  .use(reactotronRedux())
  .use(networking())
  .use(
    trackGlobalErrors({
      veto: (frame) =>
        frame.fileName.indexOf("/node_modules/react-native/") >= 0,
    })
  );
if (__DEV__) {
  reactotron.connect()
  reactotron.clear?.()
}

export default reactotron as Required<typeof Reactotron>
