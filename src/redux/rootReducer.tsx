import AsyncStorage from "@react-native-async-storage/async-storage"
import { combineReducers } from "redux"
import { persistReducer } from "redux-persist"
import accountInfoSlice from "./slices/accounInfoSlice"

// Redux persist
const rootPersistConfig = {
    key: "root",
    storage: AsyncStorage,
    whitelist: [],
}

const rootReducer = combineReducers({
    accountInfo: accountInfoSlice
})

export type RootStoreType = ReturnType<typeof rootReducer>

export type DispatchFun<P> = ({ type, payload }: Action<P>) => void
export type Action<P> = { type: string; payload?: P }

export default persistReducer(rootPersistConfig, rootReducer)
