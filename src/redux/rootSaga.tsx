import { takeLatest } from "redux-saga/effects";
import { handleGetAccountInfo, handleSetWeeklySpendingLimit } from "./saga/accountInfoHandler";
import { fetchAccountInfo, setWeeklySpendingLimit } from "./slices/accounInfoSlice";

export function* watcherSaga() {
  yield takeLatest(fetchAccountInfo, handleGetAccountInfo);
  yield takeLatest(setWeeklySpendingLimit, handleSetWeeklySpendingLimit);
}
