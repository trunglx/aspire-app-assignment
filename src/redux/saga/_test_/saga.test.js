import { handleGetAccountInfo, setWeeklySpendingLimit } from "../accountInfoHandler"
import sagaHelper from 'redux-saga-testing';

const sagaGetAccountInfo = sagaHelper(handleGetAccountInfo());
sagaGetAccountInfo('should get account info', selectResult => {
    // with Jest's `expect`
    expect(selectResult.payload.action.payload).toStrictEqual({
        availableBalance: 12343,
        weeklySpendingLimit: 1000,
        weeklySpendingLimitExhausted: 100,
        cardInfo: {
            holderName: "Trung",
            cardNumber: "1234123412341234",
            validDate: "12/20",
            cardCVV: "450"
        }
    })
});

const sagaSetWeeklySpendingLimit = sagaHelper(setWeeklySpendingLimit(2000));
sagaSetWeeklySpendingLimit('should set weekly spending limit', selectResult => {
    // with Jest's `expect`
    expect(selectResult.payload.action.payload).toStrictEqual(2000)
});

