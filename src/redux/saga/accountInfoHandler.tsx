import { call, put } from "redux-saga/effects";
import { ApiServices } from "../../data/network/ApiServices";
import { AccountInfoResponse } from "../../data/network/response/AccountInfoResponse";
import LoadingManager from "../../features/commons/components/LoadingSpinner/LoadingManager";
import { Action } from "../rootReducer";
import { GET_ACCOUNT_INFO, SET_WEEKLY_SPENDING_LIMIT } from "../slices/accounInfoSlice";

export function* handleGetAccountInfo() {
  try {
    LoadingManager.showLoading()
    const response: AccountInfoResponse = yield call(ApiServices.getMyAccountInfo);
    const { data } = response;
    console.log(response)
    yield put(GET_ACCOUNT_INFO(data));
    LoadingManager.hideLoading()
  } catch (error) {
    LoadingManager.hideLoading()
    console.log(error);
  }
}

export function* handleSetWeeklySpendingLimit({ payload }: Action<number>) {
  try {
    LoadingManager.showLoading()
    yield call(ApiServices.setWeeklySpendingLimit, payload);
    console.log(payload)
    yield put(SET_WEEKLY_SPENDING_LIMIT(payload))
    LoadingManager.hideLoading()
  } catch (error) {
    LoadingManager.hideLoading()
    console.log(error);
  }
}
