import accounInfoSlice, { SET_WEEKLY_SPENDING_LIMIT } from "../accounInfoSlice"


test('should return the initial state', () => {
    expect(accounInfoSlice(undefined, {})).toEqual(
        {
            availableBalance: 0,
            weeklySpendingLimit: 0,
            weeklySpendingLimitExhausted: 0,
            cardInfo: undefined
        }
    )
})

test('should set weekly spending limit', () => {
    expect(accounInfoSlice(undefined, SET_WEEKLY_SPENDING_LIMIT(null))).toEqual({
        availableBalance: 0,
        weeklySpendingLimit: null,
        weeklySpendingLimitExhausted: 0,
        cardInfo: undefined
    })
})