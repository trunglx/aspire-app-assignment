import { createAction, createSlice } from "@reduxjs/toolkit"
import { RootStoreType } from "../rootReducer"

const initialState: AccountInfo = {
    availableBalance: 0,
    weeklySpendingLimit: 0,
    weeklySpendingLimitExhausted: 0,
    cardInfo: undefined
}

const accountInfoSlice = createSlice({
    name: 'accountInfo',
    initialState,
    reducers: {
        GET_ACCOUNT_INFO: (state, action) => ({
            ...state,
            ...action.payload
        }),
        SET_WEEKLY_SPENDING_LIMIT: (state, action) => ({
            ...state,
            weeklySpendingLimit: action.payload
        })
    }
})

export const fetchAccountInfo = createAction('accountInfo/fetchAccountInfo');
export const setWeeklySpendingLimit = createAction('accountInfo/setWeeklySpendingLimit', (value) => ({ payload: value }));

export const { GET_ACCOUNT_INFO, SET_WEEKLY_SPENDING_LIMIT } = accountInfoSlice.actions

export const selectAccountInfo = (state: RootStoreType) => state.accountInfo
export const selectCardInfo = (state: RootStoreType) => state.accountInfo.cardInfo

export default accountInfoSlice.reducer