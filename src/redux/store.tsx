import thunk from "redux-thunk"
import { configureStore, Action } from "@reduxjs/toolkit"
import { persistStore } from "redux-persist"
import rootReducer from "./rootReducer"
import reactotron from "./ReactotronConfig"
import createSagaMiddleware from "redux-saga"
import { watcherSaga } from "./rootSaga"

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
    reducer: rootReducer,
    middleware: [thunk, sagaMiddleware],
    enhancers: (__DEV__ && [reactotron.createEnhancer()]) || undefined,
    devTools: true,
})
sagaMiddleware.run(watcherSaga);

export type AppDispatch = typeof store.dispatch

// Redux persist
const persistor = persistStore(store)

export { store, persistor }
